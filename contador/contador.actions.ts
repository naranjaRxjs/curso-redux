import { Action } from "../ngrx-fake/ngrx";

//Incrementador
export const incrementadorAction: Action = {
    type: 'INCREMENTAR',
};

//Decremantador
export const decrementadorAction: Action = {
    type: 'DECREMENTAR',
};

//Multiplicador
export const multiplicadorAction: Action = {
    type: 'MULTIPLICAR',
    payload: 3,
};

//Division
export const divisionAction: Action = {
    type: "DIVIDIR",
    payload: "2"
}
//Reset
export const resetAction: Action = {
    type: "RESET"
}