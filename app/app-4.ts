import { createStore, Store } from "redux";
import { contadorReducer } from "../contador/contador.reducer";
import { incrementadorAction, multiplicadorAction } from "../contador/contador.actions";

const store: Store = createStore( contadorReducer );

store.subscribe( ()=> {

    console.log('Subs:',store.getState())
});

store.dispatch( multiplicadorAction);
