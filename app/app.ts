

//Acciones
// la interface es una regla que un objeto debe de cumplir con determinado requisito

interface Action{
    type: string;
    payload?: any;
}

// const incrementadorAction: Action = {
//     type: 'INCREMENTAR'
// };

//REDUCER recibe 2 parametros State y Accion
function reducer( state =10, action: Action) {

   switch (action.type) {
       case 'INCREMENTAR':
           return state+=1;
        case 'DECREMENTAR':
           return state-=1;
         case 'MULTIPLICAR':
           return state * action.payload;
         case 'DIVIDIR':
           return state / action.payload;

       default:
           return state
   }
}


//Usar el reducer

//Incrementador
const incrementadorAction: Action = {
    type: 'INCREMENTAR',
};
console.log(reducer(10, incrementadorAction)); //11

//Decremantador
const decrementadorAction: Action = {
    type: 'DECREMENTAR',
};
console.log(reducer(10, decrementadorAction));

//Multiplicador
const multiplicadorAction: Action = {
    type: 'MULTIPLICAR',
    payload: 3,
};
console.log(reducer(10, multiplicadorAction));

//Division

const divisionAction: Action = {
    type: "DIVIDIR",
    payload: "2"
}
console.log(reducer(50, divisionAction));